<?php

namespace App\Services;

use App\Contact;

class ContactService
{	
	public static function findByName(string $name): ?Contact
	{
		// queries to the db
		return $name === 'tiago.perrelli' ? new Contact : null;
	}

	public static function validateNumber(string $number): bool
	{
		// logic to validate numbers

		return $number === '123456' ? true : false;
	}
}