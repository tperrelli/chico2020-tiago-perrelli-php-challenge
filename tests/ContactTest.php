<?php

namespace Tests;

use App\Call;
use App\Mobile;

use Mockery as m;

use PHPUnit\Framework\TestCase;
use App\Services\ContactService;
use App\Interfaces\CarrierInterface;

class ContactTest extends TestCase
{
    /** @test */
    public function it_returns_null_when_not_contact_is_found()
    {
        $provider = m::mock(\App\Interfaces\CarrierInterface::class);

        $contactMock = m::mock(ContactService::class);
        $contactMock
			->shouldReceive('findByName')
            ->andReturn(null);
        $contactMock->findByName('perrelli');
        
        $mobileMock = m::mock(Mobile::class, array('makeCallByName'), array($provider, $provider));

		$callMock = m::mock(Call::class);

		$mobileMock
			->shouldReceive('makeCallByName')
            ->andReturn(null);
            
        $mobileMock->makeCallByName('perrelli');

        $this->assertEquals(null, null);
    }
}