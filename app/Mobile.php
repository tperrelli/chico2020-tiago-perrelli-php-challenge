<?php

namespace App;

use App\Interfaces\CarrierInterface;
use App\Services\ContactService;

class Mobile
{
	protected $provider;
	protected $secondProvider;
	
	function __construct(CarrierInterface $provider, CarrierInterface $secondProvider)
	{
		$this->provider = $provider;
		$this->secondProvider = $secondProvider;
	}

	public function makeCallByName($name = '')
	{
		if (empty($name)) return;

		$contact = ContactService::findByName($name);

		$this->provider->dialContact($contact);

		return $this->provider->makeCall();
	}

	public function sendSMS(string $number = '', string $body = '')
	{
		if (empty($number) || empty($body)) return false;

		if (ContactService::validateNumber($number) == true) {

			$this->provider->sendSms($number, $body);
		}
	}
}
