<?php

namespace Tests;

use App\Call;
use App\Mobile;

use Mockery as m;

use PHPUnit\Framework\TestCase;
use App\Interfaces\CarrierInterface;

class MobileTest extends TestCase
{
	/** @test */
	public function it_returns_null_when_name_empty()
	{
		$provider = m::mock(\App\Interfaces\CarrierInterface::class);
		$mobile = new Mobile($provider, $provider);

		$this->assertNull($mobile->makeCallByName(''));
	}

	/** @test */
	public function it_returns_a_call_instance()
	{
		$provider = m::mock(\App\Interfaces\CarrierInterface::class);

		$callMock = m::mock(Call::class);

		$contactMock = m::mock(ContactService::class);
        $contactMock
			->shouldReceive('findByName')
            ->andReturn($callMock);
        $contactMock->findByName('tiago.perrelli');

		$mobileMock = m::mock(Mobile::class, array('makeCallByName'), array($provider, $provider));		

		$mobileMock
			->shouldReceive('makeCallByName')
			->andReturn($callMock);

		$mobileMock->makeCallByName('tiago.perrelli');
		
		$this->assertInstanceOf(Call::class, $callMock);
	}
}
